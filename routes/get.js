var express = require('express');
var router = express.Router();
var Dat = require("dat-node");

router.get("/:hex", function(req, res, next) {
  Dat(process.cwd() + '/cloned',
    {
      // 2. Tell Dat what link I want
      key: req.params.hex, // (a 64 character hash from above)
    },
    function(err, dat) {
      if (err) throw err;

      // 3. Join the network & download (files are automatically downloaded)
      dat.joinNetwork(function(err) {
        if (err) throw err;

        // After the first round of network checks, the callback is called
        // If no one is online, you can exit and let the user know.
        if (!dat.network.connected || !dat.network.connecting) {
          console.error("No users currently online for that key.");
          res.end();
        } else {
          res.end();
        }
      });
    }
  );
});

module.exports = router;

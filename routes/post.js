var express = require("express");
var router = express.Router();
var fs = require("fs-extra");
var dir = process.cwd() + "/tmp_files";
var Dat = require("dat-node");
var mirror = require('mirror-folder')
var ram = require('random-access-memory')

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Dat Project" });
});

router.post("/submit", function(req, res, next) {
  let fstream;
  req.pipe(req.busboy);
  console.log('entra');
  req.busboy.on("file", function(fieldname, file, filename) {
    console.log("Uploading: " + filename);
    console.log(process.cwd());
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    fstream = fs.createWriteStream(dir + "/" + filename);
    file.pipe(fstream);
    fstream.on("close", function() {
      Dat(dir, function(err, dat) {
        if (err) throw err;

        // 2. Import the files
        dat.importFiles();

        // 3. Share the files on the network!
        dat.joinNetwork();
        // (And share the link)
        console.log("My Dat link is: dat://" + dat.key.toString("hex"));
        // fs.removeSync(dir);
        res.render("success", { hex: dat.key.toString("hex"), title: "Dat Project" });
        // res.json(dat.key.toString("hex"));
      });
    });
  });
  // console.log(req.files);
  // res.end();
});

// router.get("/:hex", function(req, res, next) {
//   Dat('',
//     {
//       // 2. Tell Dat what link I want
//       key: req.params.hex, // (a 64 character hash from above)
//     },
//     function(err, dat) {
//       if (err) throw err;

//       // 3. Join the network & download (files are automatically downloaded)
//       dat.joinNetwork(function(err) {
//         if (err) throw err;

//         // After the first round of network checks, the callback is called
//         // If no one is online, you can exit and let the user know.
//         if (!dat.network.connected || !dat.network.connecting) {
//           console.error("No users currently online for that key.");
//           res.end();
//         } else {
//           res.end();
//         }
//       });
//     }
//   );
// });

module.exports = router;

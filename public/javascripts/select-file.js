var input = document.getElementsByClassName("inputfile")[0];
var label = document.getElementsByTagName("label")[0],
  labelVal = label ? label.innerHTML : null;
var span = document.getElementsByTagName("span")[0];
if (input) {
  input.addEventListener("change", function(e) {
    var fileName = "";
    if (this.files && this.files.length > 1) {
      fileName = (this.getAttribute("data-multiple-caption") || "").replace(
        "{count}",
        this.files.length
      );
    } else {
      fileName = e.target.value.split("\\").pop();
    }

    if (fileName) {
      span.innerHTML = fileName;
    } else {
      span.innerHTML = labelVal;
    }
  });
}
